package com.proyectos.proyecto01.controladores;

import com.proyectos.proyecto01.Util.Rutas;
import com.proyectos.proyecto01.modelo.Cliente;
import com.proyectos.proyecto01.modelo.Cuenta;
import com.proyectos.proyecto01.servicio.ServicioCliente;
import com.proyectos.proyecto01.servicio.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Rutas. CLIENTES+ "/{documento}/cuentas")
public class ControladorCuentasCliente {

    //CRUD - GET * , GET , POST, PUT , PATCH
    @Autowired
    ServicioCliente servicioCliente;
    @Autowired
    ServicioCuenta servicioCuenta;

    //POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS ---> Añade una cuenta a las cuentas del cliente

    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento , @RequestBody Cuenta cuenta){
        try{
            this.servicioCliente.agregarCuentaCliente(documento,cuenta);}
        catch (Exception e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();

    }

    @GetMapping
    public ResponseEntity<List<String>> obtenerCuentasCiente(@PathVariable String documento){
        try {
            final List<String> cuentasCliente= servicioCliente.obtenerCuentasCliente(documento);
            final ResponseEntity respuestaHttpFinal = ResponseEntity.ok().body(cuentasCliente);

            return respuestaHttpFinal;

        }catch (Exception e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
                return ResponseEntity.notFound().build();
        }
    }


    @GetMapping("/{numero}")
    public ResponseEntity<Cuenta> obtenerCuentaCliente(@PathVariable String documento, @PathVariable String numero){
        try{
            final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
            if(cliente.codigoCuentas.contains(numero)){
                final Cuenta cuenta = this.servicioCuenta.obtenerCuenta(numero);
                return ResponseEntity.ok().body(cuenta);
            }else{
                return ResponseEntity.notFound().build();
            }

        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{numero}")
    public ResponseEntity eliminarCuentaCliente(@PathVariable String documento, @PathVariable String numero){
        try{


            final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
            final Cuenta cuenta = this.servicioCuenta.obtenerCuenta(numero);

            if(cliente.codigoCuentas.contains(numero)){
                cuenta.estado="Desactivada";
//                this.servicioCuenta.emparcharCuenta(cuenta);
                cliente.codigoCuentas.remove(numero);
            }
            return ResponseEntity.noContent().build();

        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }
}
