package com.proyectos.proyecto01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Proyecto01Application {

	public static void main(String[] args) {
		SpringApplication.run(Proyecto01Application.class, args);
		System.out.println("===========================");
		System.out.println("*** APLICACION INICIADA ***");
		System.out.println("===========================");
	}

}
