package com.proyectos.proyecto01.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Cliente {

    public String documento;
    public String nombre;
    public String telefono;
    public String correo;
    public String direccion;
    public String fechaNacimiento;
    public int edad;

    //Serializacion  - Desserializacion  JSON
    @JsonIgnore

    public List<String> codigoCuentas = new ArrayList<String>();


}
