package com.proyectos.proyecto01.modelo;

public class Cuenta {

    public String numero;
    public String moneda;
    public Double saldo;
    public String tipo;
    public String estado;
    public String oficina;

}
