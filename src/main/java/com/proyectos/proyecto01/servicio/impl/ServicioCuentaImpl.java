package com.proyectos.proyecto01.servicio.impl;

import com.proyectos.proyecto01.modelo.Cuenta;
import com.proyectos.proyecto01.servicio.ServicioCuenta;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String,Cuenta> cuentas = new ConcurrentHashMap<>();

    @Override
    public void insertarCuenta(Cuenta cuenta) {
        this.cuentas.put(cuenta.numero,cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        if(!this.cuentas.containsKey(numero))
            throw new RuntimeException("No existe la cuenta: " + numero);

        return this.cuentas.get(numero);
    }

    @Override
    public void guardarCuenta(String numero,Cuenta cuenta) {
        if(!this.cuentas.containsKey(cuenta.numero))
            throw new RuntimeException("No existe la cuenta: " + cuenta.numero);

        this.cuentas.replace(cuenta.numero,cuenta);
    }

    //DELETE
    @Override
    public void borrarCuenta(String numero) {
        if(!this.cuentas.containsKey(numero))
            throw new RuntimeException("No existe el Cuenta: " + numero);
        this.cuentas.remove(numero);
    }

    @Override
    public List<Cuenta> obtenerCuentas() {
        return List.copyOf(this.cuentas.values());
    }



    //Emparchar
    @Override
    public  void emparcharCuenta(Cuenta parche){
        final Cuenta existente = this.cuentas.get(parche.numero);

        if(parche.estado!= null)
            existente.estado=parche.estado;
        if(parche.moneda!= null)
            existente.moneda=parche.moneda;
        if(parche.numero!= null)
            existente.numero=parche.numero;
        if(parche.tipo!= null)
            existente.tipo=parche.tipo;
        if(parche.saldo!= null)
            existente.saldo=parche.saldo;


        this.cuentas.replace(existente.numero,existente);
    }


}
