package com.proyectos.proyecto01.servicio;

import com.proyectos.proyecto01.modelo.Cliente;
import com.proyectos.proyecto01.modelo.Cuenta;

import java.util.List;

public interface ServicioCuenta {


    //CREATE
    public void insertarCuenta(Cuenta cuenta);

    //READ
    public Cuenta obtenerCuenta(String numero);

    //LISTAR
    public  List<Cuenta> obtenerCuentas();

    //UPDATE ( solo modificar, no crear)
    public void  guardarCuenta(String numero,Cuenta cuenta);

    //EMPARCHAR
    public  void emparcharCuenta(Cuenta cuenta);


    //DELETE
    public void borrarCuenta(String numero);





}
